# haveweplayed.sh

To find out how many times and when we played a particular artist or track (or anything). Creates a file (in "results/") with all the playlist lines where the searched term appears.

For example:

`bash haveweplayed.sh "Autechre"`
`bash haveweplayed.sh "laurel halo"`

The search is case-insensitive.
